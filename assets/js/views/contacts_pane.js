define([
    'jquery',
    'underscore',
    'marionette',
    'views/contact_item',
    'jquery-dragscrollable'
], function($, _, Marionette, ContactItem) {
    return Marionette.CompositeView.extend({
        itemView: ContactItem,
        template: "#contacts-pane-template",
        groupTemplate: Marionette.TemplateCache.get("#contacts-group-template"),
        ui: {
            "list": "#contacts__scroller",
            "wrapper": "#contacts__wrapper"
        },
        initialize: function(options) {
            this._onScroll = _.bind(this._onScroll, this);
            this._contactMapper = options.contactMapper;
        },
        onRender: function() {
            this.ui.wrapper.on("scroll", this._onScroll);
            this.ui.wrapper.dragscrollable();
        },
        _onScroll: function(event) {
            var self = this,
                groupsStorage = self._groupsStorage,
                target = event.currentTarget,
                scrollTop = target.scrollTop,
                cached,
                next,
                offsetTop,
                headerHeight = 20;

            for (var firstChar in groupsStorage) {
                cached = groupsStorage[firstChar].closest("li");
                offsetTop = cached[0].offsetTop;
                if (offsetTop < scrollTop) {
                    next = cached.next();
                    if (next && next[0].offsetTop - headerHeight < scrollTop) {
                        cached
                            .find(".contacts__group__first_char")
                            .css("transform", "translateY(" + (cached.outerHeight() - headerHeight) + "px)");
                    } else {
                        cached
                            .find(".contacts__group__first_char")
                            .css("transform", "translateY(" + (scrollTop - offsetTop) + "px)");
                    }
                } else {
                    cached
                        .find(".contacts__group__first_char")
                        .css("transform", "");
                }
            }

        },
        onCompositeCollectionBeforeRender: function() {
            // Очистим обьект который хранит сгруппированные по первой букве списки
            this._groupsStorage = {};
        },
        onBeforeRender: function() {
            // Очистим обьект который хранит сгруппированные по первой букве списки
            this._groupsStorage = {};
        },
        onBeforeClose: function() {
            // Удалим обьект который хранит сгруппированные по первой букве списки
            delete this._groupsStorage;
        },
        appendHtml: function(compositeView, itemView, index) {
            var firstCharOfName = this._contactMapper(itemView.model.get("name")),
                container;

            // если в обьекте который хранит сгруппированные по первой букве списки, еще нет списка с буквой
            // firstCharOfName добавим его
            if (!(container = compositeView._groupsStorage[firstCharOfName])) {
                container = $(compositeView.groupTemplate({
                    firstChar: firstCharOfName
                }));
                compositeView.ui.list.append(container);
                container = container.find(".contacts");
                compositeView._groupsStorage[firstCharOfName] = container;
            }
            container.append(itemView.el);
        }
    });
});