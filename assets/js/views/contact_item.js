define([
    'marionette'
], function(Marionette) {
    return Marionette.ItemView.extend({
        template: "#contact-template",
        tagName: "li"
    });
});