define([
    'marionette',
    'collections/contacts',
    'views/contacts_pane'
], function(Marionette, Contacts, ContactPane) {
    var app = new Marionette.Application;

    app.on("initialize:before", function() {
        app.addRegions({
            "mainRegion": "#main__region"
        });
    });

    app.addInitializer(function(options) {
        options || (options = {});

        var collection = new Contacts(options.contacts || []);
        var view = new ContactPane({
            collection: collection,
            contactMapper: options.contactMapper
        });
        app.getRegion("mainRegion").show(view);
        view.render();
    });

    return app;
});