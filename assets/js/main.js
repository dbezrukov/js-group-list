require.config({
    waitSeconds: 30,
    baseUrl: './assets/js/',
    paths: {
        'jquery': 'bower_components/jquery/jquery',
        'jqueryui': 'bower_components/jquery-ui/ui/jquery-ui',
        'underscore': 'bower_components/underscore/underscore',
        'backbone': 'bower_components/backbone/backbone',
        'backbone.wreqr': 'bower_components/backbone.wreqr/lib/amd/backbone.wreqr.min',
        'backbone.babysitter': 'bower_components/backbone.babysitter/lib/amd/backbone.babysitter.min',
        'marionette': 'bower_components/marionette/lib/core/amd/backbone.marionette.min',
        'jquery-dragscrollable': 'vendors/dragscrollable'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette': {
            deps: ['backbone', 'underscore', 'jquery'],
            exports: "Marionette"
        },
        'underscore': {
            exports: '_'
        },
        'jquery-dragscrollable': {
            deps: [ 'jquery' ],
            exports: 'jQuery.fn.dragscrollable'
        }
    }
});

require([
    'jquery',
    'app',
    'data'
], function($, app, data) {
    $(function() {
        app.start({
            contacts: data.contacts,
            contactMapper: data.contactMapper
        });
    });
 });